'use strict';

/**
 * @ngdoc function
 * @name ebaySearchingApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ebaySearchingApp
 */
angular.module('ebaySearchingApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
