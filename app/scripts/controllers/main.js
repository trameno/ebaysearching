'use strict';

/**
 * @ngdoc function
 * @name ebaySearchingApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ebaySearchingApp
 */
// angular.module('ebaySearchingApp')
// 	.controller('MainCtrl', function ($scope) {
// 	 	$scope.awesomeThings = [
// 	 	'HTML5 Boilerplate',
// 	 	'AngularJS',
// 	 	'Karma'
//  	];
// });

angular.module('ebaySearchingApp')
	.controller('MainCtrl', function() {
 	var vm = this;
 	vm.searchInput = '';

 	vm = {};
 	vm.search = function(){
 		var searchInput = vm.product;
 		var searchOrder = vm.order;
 		var state = vm.state;
 		var conditions = vm.conditions;

 		if(searchOrder==='1'){
 			searchOrder = 'PricePlusShippingLowest';
 		}else{
 			searchOrder = 'CurrentPriceHighest';
 		}

 		var url = 'http://svcs.ebay.com/services/search/FindingService/v1';
 		url += '?OPERATION-NAME=findCompletedItems';
 		url += '&SERVICE-VERSION=1.7.0';
 		url += '&SECURITY-APPNAME=Trameno69-5e60-4a42-ac96-7ac9cf73e41';
 		url += '&GLOBAL-ID=EBAY-'+state;
 		url += '&RESPONSE-DATA-FORMAT=JSON';
 		url += '&callback=cbFindItemsByKeywords';
 		url += '&REST-PAYLOAD';
 		url += '&keywords='+searchInput;
 		url += '&itemFilter(0).name=Condition';
 		url += '&itemFilter(0).value='+conditions;
        // url += "&itemFilter(1).name=Currency";
        // url += "&itemFilter(1).value=EUR";
        // url += "&itemFilter(2).name=SoldItemsOnly";
        // url += "&itemFilter(2).value=true";
      url += '&sortOrder='+searchOrder;
      url += '&paginationInput.entriesPerPage=100';

      var s = document.createElement('script');
		s.src = url;
      document.body.appendChild(s);
   };

  });

angular.module('ebaySearchingApp')
	.controller('paginationCtrl', ['$scope', function (scope) {
		var
		nameList = ['Pierre', 'Pol', 'Jacques', 'Robert', 'Elisa'],
		familyName = ['Dupont', 'Germain', 'Delcourt', 'bjip', 'Menez'];

		function createRandomItem() {
			var
			firstName = nameList[Math.floor(Math.random() * 4)],
			lastName = familyName[Math.floor(Math.random() * 4)],
			age = Math.floor(Math.random() * 100),
			email = firstName + lastName + '@whatever.com',
			balance = Math.random() * 3000;

			return{
				firstName: firstName,
				lastName: lastName,
				age: age,
				email: email,
				balance: balance
			};
		}	

		scope.itemsByPage=15;

		scope.rowCollection = [];
		for (var j = 0; j < 200; j++) {
			scope.rowCollection.push(createRandomItem());
	}
}]);

// Parse the response and build an HTML table to display search results
// function cbFindItemsByKeywords(root) {
// 	var items = root.findCompletedItemsResponse[0].searchResult[0].item || [];

// 	var html = [];
// 	html.push('<table width="100%" border="0" cellspacing="0" cellpadding="3"><tbody>');
// 	for (var i = 0; i < items.length; ++i) {
// 		var item     = items[i];
// 		var title    = item.title;
//         // var ebayNumber = item.itemId[0];
//         // var condition = item.condition[0].conditionDisplayName[0];
//         // var location = item.location[0];
//         // var shippingCost = '';
//         // if(item.shippingInfo[0].shippingServiceCost!==undefined){
//         //     shippingCost = item.shippingInfo[0].shippingServiceCost[0].__value__;
//         // }
//         // var startDate = item.listingInfo[0].startTime
//         // var endDate = item.listingInfo[0].endTime;
//         var price    = item.sellingStatus[0].currentPrice[0].__value__;
//         var pic      = item.galleryURL;
//         var viewitem = item.viewItemURL;
        
//         // if (null != title && null != viewitem) {
//         // 	html.push('<tr><td>' + '<img src="' + pic + '" border="0">' + '</td>' +
//         // 		+ '<td><a href="' + viewitem + '" target="_blank">'+ title + '</a></td>'
//         //         // + '<td>' + ebayNumber + ' - '+'</td>'
//         //         // + '<td>' + condition + ' - '+'</td>'
//         //         // + '<td>' + location + ' - '+'</td>'
//         //         // + '<td>' + shippingCost + ' - '+'</td>'
//         //         + '<td>' + price + ' € - </td>'
//         //         // + '<td>' + startDate + ' - '+'</td>'+ '<td>' + endDate + '</td>'
//         //         + '</tr>');
//         // }
//      }
//      // html.push('</tbody></table>');
//      // document.getElementById("results").innerHTML = html.join("");
//   }
