'use strict';

/**
 * @ngdoc overview
 * @name ebaySearchingApp
 * @description
 * # ebaySearchingApp
 *
 * Main module of the application.
 */
angular
  .module('ebaySearchingApp', [
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'smart-table'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
